const io = require('socket.io')(process.env.PORT || 3000)
const _ = require('lodash')
let arrUsers = []
console.log('Server start on Port : ',process.env.PORT || 3000)
let phong
io.on('connection', socket => {
    const id = _.get(socket,'handshake.query.token')
    const name = _.get(socket,'handshake.query.name')
    const newuser = {id, name, idSocket: socket.id}
    const checkExist = _.find(arrUsers,{id})

    console.log('new connection', newuser)


    console.log(socket.adapter.rooms)

    if (!checkExist) {
        console.log('New connection : ',newuser)
        arrUsers.push(newuser)
        socket.emit('connect_success', 'success')
        socket.broadcast.emit('HAS_NEW_USER', newuser)
    } else {
        console.log('user existed !')
    }

    socket.on('get_list_users_online', (callback) => {
        console.log('get_list_users_online')
        callback(arrUsers)
    })

    socket.on('createRoom', id => {
        socket.join(id)
        phong = id
        io.sockets.in(id).emit('callTo', {...newuser, idCall: id})
        console.log('rooms : ',socket.adapter.rooms)
    })

    socket.on('allowCall', PeerId => {
        console.log('allowCall :',PeerId)
        io.sockets.in(phong).emit('allowCallResponse', PeerId)
    })

    socket.on('cancalCall', PeerId => {
        console.log('cancalCall :',PeerId)
        io.sockets.in(phong).emit('cancalCallResponse', PeerId)
    })

    socket.on('endCall', PeerId => {
        console.log('endCall :',PeerId)
        io.sockets.in(phong).emit('endCallResponse', PeerId)
    })

    socket.on('disconnect', ()=>{
        _.remove(arrUsers,function(n){
            return n.idSocket === socket.id
        })
        io.emit('USER_DISCONNECT',arrUsers)
        console.log('user disconnect', newuser)
        console.log('arrUsers', arrUsers)
    })
})
